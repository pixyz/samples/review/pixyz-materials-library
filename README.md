# Materials Library

![](doc/header.png)

This sample lets you take advantage of Physically Based Rendering (PBR) materials inside Pixyz.

# Getting Started

## Compatible Software

![](doc/puce-review-64x64.png)|![](doc/puce-studio-64x64.png)
:--------------:|:--------------:
Review | Studio

## Set up

1. Clone or download project
2. Unzip the materials archive
3. Import or load as sub scene the .pxz file in your favorite software